﻿using System;
using System.Text;

namespace TextTransformer
{
    class Program
    {
        public static string testString = "Напрасно я бегу к сионским высотам,\nГрех алчный гонится за мною по пятам…\nГак, ноздри пыльные уткнув в песок сыпучий,\nГолодный лев следит оленя бег пахучий.\n    \t     ";
        public static string testString2 = "Test";

        public abstract class TextTransformer
        {
            static public string ToUpper(string s)
            {
                StringBuilder res = new StringBuilder();
                foreach (string s1 in s.Split('\n'))
                    if (s1.Trim().Length == 0)
                        res.Append("Void string\n");
                    else
                        res.Append(s1.ToUpper()+'\n');
                res.Remove(res.Length - 1, 1);
                return res.ToString();
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine(TextTransformer.ToUpper(testString));
            Console.WriteLine(TextTransformer.ToUpper(testString2));
            Console.ReadLine();
        }
    }
}
